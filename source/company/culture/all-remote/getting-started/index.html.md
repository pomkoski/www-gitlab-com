---
layout: markdown_page
title: "Guide for starting a remote job"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing considerations and tips for starting a new remote role. 

## Take time to internalize values

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/oRbEuSYwBAg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, two colleagues discuss the importance of living out the GitLab values.* 

Absorbing a company culture in a colocated setting occurs over time, as one witnesses behaviors that are supported, praised, and discouraged. A benefit to working in an all-remote setting is that culture is (ideally) documented.

As you settle into a new role, ensure that you devote time to reading and internalizing [company values](/handbook/values/). While this may feel like a skippable activity, understanding the values early on enables you to have a strong foundation on which to build. Every decision you make in your role should be guided by values. If you have questions about how values are lived, take time during onboarding to consult with others.

At GitLab, one's [Onboarding Buddy](/handbook/general-onboarding/onboarding-buddies/) is there to answer these questions and provide guidance. There is also a `#values` Slack channel for related discussions. Whenever you see a value being lived, we encourage the use of a [values emoji reaction](/handbook/communication/#say-thanks) — ![CREDIT emoji](https://about.gitlab.com/images/handbook/credit-emoji.png) — to reinforce values being used day-to-day.

## Adjusting to a self-service mindset

GitLab's [100% remote culture](/company/culture/all-remote/) and our workplace methodologies are highly unique. You should not expect to transfer the norms of colocated corporations into a work from anywhere scenario. Those who thrive at GitLab take the opportunity to drop prior workplace baggage at the door, embrace a liberating and empowering [set of values](/handbook/values/), and give themselves permission to truly operate differently. 

So differently, in fact, that many of GitLab's most effective processes would be discouraged or forbidden in conventional corporations. It's not a trap. It's the future of work. 

Explore the resources in our [all-remote handbook section](/company/culture/all-remote/self-service), as well as our collection of [relevant blog posts](/blog/categories/culture/), for a deeper understanding of [life at GitLab](/company/culture/#life-at-gitlab).

## Focus your workspace

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Kf0QOihrxLg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO Sid Sijbrandij discusses his remote work set-up with CMO Todd Barr.* 

If you've managed to be productive despite operating within a conventional [open office floor plan](https://royalsocietypublishing.org/doi/full/10.1098/rstb.2017.0239), you'll likely be thrilled with your productivity level [as a remote team member](/blog/2019/08/15/all-remote-is-for-everyone/). 

However, engineering an [optimal workspace](/company/culture/all-remote/workspace/) may not come naturally — particularly if you've worked in environments which were defined prior to you joining. 

For new remote workers, it's important to think about [where you prefer to work on a daily basis](/blog/2019/09/12/not-everyone-has-a-home-office/). Optimize this for focus. Consider spaces in your home, coworking venue, etc. where you will be largely free from distraction. Given that most offices are riddled with distractions, the ability to design a space that dodges them [is a boon for remote workers](/blog/2018/05/17/eliminating-distractions-and-getting-things-done/). 

The more focused you are due to your surroundings, the more quickly you can accomplish your duties and move on to important non-work activities. 

Pay close attention to ambient sounds, visual distractions, and areas of high traffic. Aim to dedicate a space where only work occurs, enabling you to focus specifically on work while there and [healthily disconnect](/blog/2019/07/09/tips-for-working-from-home-remote-work/) when you exit. 

## Establish routine with family and friends

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BC7Prkm3v3g" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, two GitLab colleagues discuss the benefits of living in a lower cost-of-living environment near friends, family, and community.*

When you physically leave a home to go to work, it signals to friends and family that you are not easily reachable, and that you are focused on work-related tasks. In a [work from anywhere scenario](/blog/2018/05/11/day-in-life-of-remote-sdr/), you're able to remain at home all day should you so choose. 

For families who are not used to this, boundaries can be difficult to establish and maintain. When an employee is visibly at home, it may signal to other family members that they are accessible. 

Consider having an open, honest conversation with family members about your working hours. Explain that while you are indeed home, you should be considered unreachable unless there's a significant level of need that would roughly align with justifying an office interruption in a colocated setting. 

This tends to be [particularly vital to discuss with children](/blog/2019/08/01/working-remotely-with-children-at-home/), who may struggle to understand why a working parent is in the home but unable or unwilling to engage with them for certain parts of the day. 

By explaining that [a working parent](/blog/2019/07/25/balancing-career-and-baby/) is able to spend more time engaging immediately prior to and after work due to dropping the commute, it helps frame the new scenario in a way that spotlights the benefits to a child. To take this a step further, consider [arranging your work schedule](/blog/2019/06/18/day-in-the-life-remote-worker/) to allow for a midday activity with a child. By engaging in this manner, and explaining that such a luxury would have been impossible in a colocated setting, it can help reinforce the benefits of remote work.

Consider [visual availability indicators](/company/culture/all-remote/workspace/#busyavailable-indicators) that will allow friends and family to easily know whether or not you're willing to be contacted during working hours. 

## Experiment with work environments 

While working from home is an ideal scenario for many, you may find that you're happier, more fulfilled, and more productive elsewhere. If you feel burdened or socially drained while working from home, consider a coworking space or an [outdoor area](/blog/2019/09/23/how-to-push-code-from-a-hammock/). 

Preferences vary from person to person, and even from season to season. For example, you may find that [working while road-tripping in an RV](/blog/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/) is exactly the kind of environment to maximize your personal and professional happiness. 

Or, you may find that [working with other nomads in a foreign location](/blog/2019/09/04/not-all-remote-is-created-equal/) provides added clarity and creativity.

People [adopt a remote lifestyle](/company/culture/all-remote/people/) for many reasons. It's important to consider unique work environments if you're new to a remote role. If you've been forced into an office for your entire career, you may not instinctively know which work environment is best for you. By experimenting and asking for advice from others who have worked remotely before, you may discover a side of yourself that's finally able to flourish due to newfound flexibility. 

At GitLab, team members are allowed to [expense costs](/handbook/spending-company-money/) associated with coworking spaces or external offices, as we recognize that working out of one's home is not always ideal or desired. In you're new to a remote role, ask your manager if external office space is reimbursable.

## Prioritize ergonomics

Whether it's in your home office, at a coffee shop, a coworking space, or elsewhere, [consider ergonomics in every instance](/company/culture/all-remote/workspace/#ergonomic-considerations). 

Pay close attention to seating and posture, as well as repetitive stress on arms and wrists. It's worth investing in equipment designed to help you work healthier. Cutting corners in this area now can lead to chronic pains in the future. 

At GitLab, team members are [allowed to spend company money as they would their own](/handbook/spending-company-money/). If you're new to a remote role, ask your manager if ergonomic equipment is reimbursable.

## Procure the right equipment

Working remotely enables you to work from all types of environments. Those new to remote working are unlikely to have [equipment](/company/culture/all-remote/workspace/) optimized for that flexibility. 

Items such as standing/treadmill desks, standing mats, external keyboard/mice, high-definition webcams, webcam lighting, noise-cancelling headphones, a dedicated microphone, a laptop riser, and ergonomic cases/backpacks should be considered depending on your work scenario. 

## Bolster your internet connection, and consider redundancy

It's important to remember that remote workers are responsible for their own connectivity. This may require you to invest in a more substantial home internet connection. It's worth consider redundancy in this department as well. For example, upgrading your smartphone plan to support tethering or investing in a dedicated mobile hotspot or MiFi. This ensures that you have a fallback connection in place should your primary connection fail. 

Secondary connections are particularly important when multiple family members work from home. For example, if one member is uploading a large file, that may impact the audiovisual quality of another member's [video call](/blog/2019/08/05/tips-for-mastering-video-calls/). A secondary connection allows each working member to operate on their own without impacting the bandwidth of the other. 

## Manage isolation

For those accustomed to social interactions within a colocated work setting, it can be jarring to move into a remote environment where you primarily work alone. 

It's important to pay close attention to your [emotional health](/blog/2018/03/08/preventing-burnout/) as you transition to a remote role. If you sense a void from missing out on face-to-face interaction, act deliberately and early. 

Here are a few suggestions to avoid isolation.

1. Schedule breaks in your day to interact with friends or family in the home, or nearby in your community.
1. Experiment with working in a shared setting (e.g. a local coworking venue, [WiFi Tribe](/blog/2019/09/04/not-all-remote-is-created-equal/), etc.).
1. Leverage [video calls](/blog/2019/08/05/tips-for-mastering-video-calls/) whenever practical, both at work and outside of work.
1. Engage with colleagues on [non-work topics](/handbook/communication/chat/#social-groups), via Slack, ad hoc video calls, or other communication tools.
1. Prioritize work events where travel and physical interaction is involved (e.g. [company summits](/events/gitlab-contribute/), meetups, [events](/company/culture/all-remote/events/), conferences, etc.).

## Be intentional about informal communication

![Spreading aloha on a GitLab company call](/images/all-remote/GitLab-Aloha-Shirt-Team.jpg){: .shadow.medium.center}
Spreading aloha on a GitLab company call
{: .note.text-center}

Lean in when it comes to [informal communication](/company/culture/all-remote/informal-communication/). While this requires the use of an unusual muscle — particularly for those who have not worked in a remote setting before — it's important to form and foster relationships.

Making social connections with coworkers is vital to building trust within your organization. One must be intentional about designing informal communication when it cannot happen organically in an office.

If your new remote role does not specify informal communication options available to you, consider [reading GitLab's list](/company/culture/all-remote/informal-communication/) and implementing at your company. 

## Imagine what's possible with no commute 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gyDGeibTfdk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, two GitLab colleagues discuss the benefits of all-remote. In particular, the ability to travel on a continual basis while working and visiting colleagues, friends, and family.*

Perhaps the most inspiring and thrilling aspect of embracing a remote role is [the sheer potential of it](/blog/2017/01/31/around-the-world-in-6-releases/). The possibilities are near limitless when you're [empowered](/blog/2017/06/30/there-and-back-again-in-one-release/) to live and work where you are most fulfilled. 

While traveling the globe or working from a beachside cafe may not appeal to everyone, remote work enables a dramatically different lifestyle for all. By removing one's commute and ditching the requirement to be seen in a physical office, you're able to **structure your work around your life** as opposed to the other way around. 

This is a [profound shift](/company/culture/all-remote/people/), and it may not be entirely obvious how to maximize one's new reality. 

Consider starting a document listing things that working within a colocated environment prevents you from doing. Items such as dropping children off for school, participating in midday community service functions, visiting friends and family outside of peak holiday seasons, learning a new language by living in a foreign locale, relocating to care for an ill relative, moving to a more satisfying environment, working in a more comfortable wardrobe, etc. 

By taking a look at what you've been missing, you're able to more easily assemble a plan for embracing such things in a remote setting. 

Consider asking yourself what you'll do with the time you save by losing the commute. Perhaps you'll be inspired to embrace a daily fitness routine, cook at home, or spend additional time with friends, family, and community. There's nothing wrong with reclaiming that time and using it to bolster your overall wellness, from improving your sleep habits to furthering your education.

## Ask questions and learn from others

If you're new to working remotely, don't be afraid to ask others within your company for advice, or even for someone to serve as a mentor as you acclimate. 

Too, be proactive in asking questions and finding remote communities to learn from. Several of these are listed below.

1. [LinkedIn Digital Nomads and Remote Workers group](https://www.linkedin.com/groups/13657237/)
1. [Reddit Remote Work](https://www.reddit.com/r/remotework/)
1. [WeWorkRemotely community](https://weworkremotely.com/)

Networking can be more difficult when working remotely, so it's important to [consider remote work conferences and summits](/company/culture/all-remote/events/).

## Resist the urge to operate the same as before

<!-- blank line -->

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/cy6WGuzArgY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*Emna G., founder and CEO at [Veamly](https://veamly.com/), speaks with GitLab's [Darren M.](https://twitter.com/darrenmurph) on a number of remote work topics: reinforcing culture, encouraging work-life harmony, remote work processes, the importance of process optimization in team productivity, and asynchronous communication.*

*You'll find more videos such as these in the [Remote Work playlist on the GitLab Unfiltered YouTube channel](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

As aptly stated in [Basecamp's Handbook](https://basecamp.com/handbook), **there’s as much to unlearn as there is to learn** when it comes to thriving in a remote role.

Remote work enables a great deal of flexibility, freedom, and autonomy. It also requires adaptibility when it comes to communication, and may require experimentation by the team member to reach peak enjoyment. 

It can be tempting for new remote workers to simply implement tactics used in colocated spaces, but from their home or a coworking space. For example, learning to search for answers within [documentation](/handbook/documentation/) rather than tapping someone on the shoulder and asking a question. 

This also applies to more nuanced aspects of work, including employee perception. Whereas working from a unique place or doing things differently may be discouraged in a colocated space, consider celebrating such diversity in a remote space. 

There is more benefit to working remotely than simply losing the commute. Those new to this paradigm should give themselves permission to explore, experiment, and learn from others both on their team and on remote-centric forums. 

----

Return to the main [all-remote page](/company/culture/all-remote/).
