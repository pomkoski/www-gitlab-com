---
layout: handbook-page-toc
title: Learning Sessions
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Learning Sessions page! Here you will find the schedules for upcoming learning sessions as well as links to content for past learning sessions.  

## Live Learning Schedule

1. The 2020 Live Learning schedule is as follows: 
   - January - TBC
   - February - TBC
   - March - TBC
   - April - TBC
   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC

## Past Live Learning Sessions
### 2019
1. November - [Communicating Effectively & Responsibly Through Text](/company/culture/all-remote/effective-communication/)
1. December - [Inclusion Training](https://www.youtube.com/watch?v=gsQ2OsmgqVM&feature=youtu.be)

## Action Learning Schedule

1. The 2020 Action Learning schedule is as follows: 
   - January - TBC
   - February - TBC
   - March - TBC
   - April - TBC
   - May - TBC
   - June - TBC
   - July - TBC
   - August - TBC
   - September - TBC
   - October - TBC
   - November - TBC
   - December - TBC

## Past Action Learning Sessions
### 2019
1. November - Annual Compensation Review 
