!function(){

  var loopcount;
  var attemptedAction;
  var extraClassnames = "";

  function getCookie(name) {
    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
    return v ? v[2] : "";
  }

  var renderMessageStandard = function(loopcount, targets, attemptedAction, extraClassnames, messageLocation)
  {
    var CBNCMessage  = '<div class="cbnc-message' + extraClassnames + '">Trouble ' + attemptedAction + '? You may need to update your <a href="javascript:Cookiebot.renew();">cookie settings</a> to allow <strong>personalization</strong> cookies.</div>';
    var wrapper = document.createElement('div');
    wrapper.setAttribute('class', 'cbneedsconsent');
    targets[loopcount].parentNode.insertBefore(wrapper, targets[loopcount]);
    wrapper.appendChild(targets[loopcount]);
    if(messageLocation == "before") 
    {
      wrapper.insertAdjacentHTML('afterbegin', CBNCMessage);
    };
    if(messageLocation == "after") 
    {
      wrapper.insertAdjacentHTML('beforeend', CBNCMessage);
    };
  }

  isCookieConsentRequired = getCookie("CookieConsent");
  if (isCookieConsentRequired !== "-1")
  /* if consent is required */
  {
    if (isCookieConsentRequired.indexOf("marketing:true") == -1)
    /* if personalization is not currently enabled */
    {
      // site header search
      var targets=document.querySelectorAll('header .search-box');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "using search";
        extraClassnames = " cbnc-search-header";
        messageLocation = "after";
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation);
      };
      // handbook search
      var targets=document.querySelectorAll('#search-handbook');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "using search";
        extraClassnames = " cbnc-search-handbook";
        messageLocation = "before";
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation);
      };
      // youtube videos
      var targets=document.querySelectorAll('iframe[data-src*="youtube"]');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "viewing this video";
        extraClassnames = "";
        messageLocation = "before";
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation);
      };
      // sched event calendars
      var targets=document.querySelectorAll('iframe[data-src*="calendar"]');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "viewing this calendar";
        extraClassnames = "";
        messageLocation = "before";
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation);
      };
      // marketo gated content forms
      var targets=document.querySelectorAll('.form-to-resource-content form, .sales form');
      for(loopcount=0;loopcount<targets.length;loopcount++)
      {
        attemptedAction = "with this form";
        extraClassnames = "";
        messageLocation = "before";
        renderMessageStandard(loopcount, targets, attemptedAction, extraClassnames, messageLocation);
      };
    }
  };

  // because cookiebot intercepts the load event, we have to start it again to fix other scripts...
  dispatchEvent(new Event('load'));

}();