---
layout: job_family_page
title: "Marketing Web Developer/Designer"
---

As the Marketing Web Developer/Designer you will work closely with product marketing, content marketing, and other members of the Marketing team. This role will be part of the Marketing Design team and report directly to the CMO.

## Responsibilities

* Lead design and development (HTML, CSS, JavaScript) of [about.gitlab.com](/), emails, and standalone landing pages.
* Develop & maintain a top-notch user experience across all of these properties through A/B testing and data-driven design.
* Research, design, and implement concepts that increase engagement and conversion.
* Define front-end framework(s), UX style guides, and user flows to drive efficiency and consistency in our user experience.

## Requirements

* 6+ years experience specializing in front-end development, web and user experience design.
* A professional portfolio demonstrating a strong background in front-end development, web, UX, and visual design (candidates without a portfolio will not be considered).
* Expert-level understanding of responsive design and best practices.
* Strong working knowledge of HTML, CSS, and JavaScript (jQuery).
* Experience running effective A/B tests.
* Strong knowledge of design tools (Sketch, Adobe CC, etc.).
* You use research and best practices to create, validate, and present your ideas to project stakeholders.
* Obsessive over the details and creating thoughtful and intuitive experiences.
* Able to iterate quickly and embrace feedback from many perspectives.
* Strong understanding of information architecture, interaction design, and user-centered design best practices.
* Understanding of Git and comfortable using the command line.
* A track record of being self-motivated and delivering on time.
* Candidates in Americas timezones will be preferred.
* Ability to use GitLab

### Nice-to-haves

* Basic Ruby knowledge; our website is built on Middleman using HAML.
* Able to pitch in on other design efforts; i.e. branding, illustration, swag, social ad campaigns, digital publications, etc.
* Experience working in a fully or partially remote company.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Portfolios of qualified candidates will be reviewed by our hiring team
* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* First, candidates will be invited to meet with our Designer
* Second, candidates will be invited to complete a design/development challenge
* Third, candidates will be invited to meet with one of our Frontend Engineers
* Fourth, candidates will be invited to meet with one of our Product Designers
* Successful candidates will then be invited to schedule an interview with our CMO
* Finally, successful candidates may be asked to interview with our CEO
